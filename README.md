# Proj0-Hello
-------------
This project uses ```configparser``` to read user configurations from ```credentials.ini``` in order to print a message.  

## Author:
Alec Springel  
aspring6@uoregon.edu  

# Usage
---------------
To run the program, configure credentials.ini, and run:  
```python3 hello.py```